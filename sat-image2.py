### Make data image 2 
from datetime import datetime, timezone
import cartopy.feature as cfeature
import cartopy.crs as ccrs
import matplotlib.pyplot as plt
from metpy.plots import ctables
from metpy.units import units
import metpy.calc as mpcalc
from netCDF4 import num2date
import numpy as np
from scipy.ndimage import gaussian_filter
#from siphon.catalog import TDSCatalog
import xarray as xr
from cartopy import crs
from wrf import latlon_coords, ll_to_xy, xy_to_ll
#from pyproj import Proj
import pyproj as proj
from netCDF4 import Dataset

dataFile = '/data/jpk237/goes16-data/OR_ABI-L1b-RadF-M6C16_G16_s20192580300174_e20192580309493_c20192580309567.nc'
ds = xr.open_dataset(dataFile)
ncfile = Dataset(dataFile)
rad = ds.metpy.parse_cf('Rad')

merc = crs.Mercator.GOOGLE
dataproj = rad.metpy.cartopy_crs



# Grab coordinate data
x = rad.x
y = rad.y

lat1 = 20; lon1 = -80;
lat2 = 5; lon2 = -15;
coord_1 = [lat1, lon1]
coord_2 = [lat2, lon2]

#x_y = ll_to_xy(ncfile,lat1,lon1)

crs_bng = proj.Proj('epsg:27700')  # <--- this one seems to be the right transfrom 

x_c1, y_c1 = crs_bng(coord_1[1], coord_1[0])
x_c2, y_c2 = crs_bng(coord_2[1], coord_2[0])
p = proj.Proj('epsg:3857')
x3, y3 = p(coord_1[1], coord_1[0])
x4, y4 = p(x[0],y[0])

p2 = proj.Proj(dataproj.proj4_params)
x5,y5 = p2(lon1,lat1)
#p = proj.Proj('epsg:3857')
#x_c1, y_c1 = p(coord_1[1], coord_1[0])
#x_c2, y_c2 = p(coord_2[1], coord_2[0])
#crs_wgs = proj.Proj('epsg:4326')

#x3,y3 = proj.transform(crs_wgs,crs_bng,lon1,lat1)
#x4, y4 = proj.transform(crs_wgs,p,lon1,lat1)

#x_y = ll_to_xy(coord_1)

#### find closest x/y values to use for bounds

# Gets infrared values
ir_rad = ds.Rad
# save needed values
fk1 = ds.planck_fk1; fk2 = ds.planck_fk2
bc1 = ds.planck_bc1; bc2 = ds.planck_bc2
# calculates brightness temp
#T = fk2 / (np.log((fk1/ir_rad) + 1))
# add scale and offset corretion
#ir_BT = bc2*T + bc1

def getXY(Var):
    lats, lons = latlon_coords(Var)
    return lats, lons

lats, lons = getXY([x.values,y.values])

#ax.set_extent([-135,-60,10,65], crs=ccrs.PlateCarree())