## Hurricane data for machine learning dataset
from datetime import datetime
import cartopy.crs as ccrs
import matplotlib.pyplot as plt
import numpy as np
import xarray as xr
import cartopy.feature as cfeature
from cartopy.feature import NaturalEarthFeature
import matplotlib.gridspec as gridspec
from netCDF4 import Dataset
from matplotlib.cm import get_cmap
import pytz
from datetime import datetime, timezone
from os import path
import re
from boto3.session import Session
import os.path
from os import path
import json
import pickle

aws = Session()
s3 = aws.resource('s3')
noaa_goes16 = s3.Bucket('noaa-goes16')
noaa_goes16.creation_date

########## Function to get goes imagery ############
def get_goesFile(pref):
    counter = 0
    dcount = 0
    ### Grabs all goes16 files under time 'pref'---> fro IBtRACS hour datapoint, now have 10/15 minute imagery
    for nc_file in noaa_goes16.objects.filter(Prefix=pref):
        goes16_datetime = re.compile(r'OR_ABI-L1b-RadF-M\dC16_G16_s(\d{14})_e\d{14}_c\d{14}\.nc')
        random_goes16 = nc_file.key
        current_filename = path.basename(random_goes16)     # converts nc_file.key to actual file's name
        
        if None != goes16_datetime.search(current_filename) and counter == 0:
            goes_groups = goes16_datetime.search(current_filename).groups()
            goes_scan_start = goes_groups[0][:-3]
            goes_scan_start = datetime.strptime(goes_scan_start, '%Y%j%H%M').replace(tzinfo=timezone.utc)
            
            if counter == 0:
                # inputs first goes file into "s3_file" dictionary for time 't'
                strm_Dict[name][marker]['s3_file'] = current_filename
                counter = counter + 1   # no longer on 1st file
            
            # adds s3 file names to 'goes16_files'
            if type(strm_Dict[name]['goes16_files']) == list:
                strm_Dict[name]['goes16_files'].append(current_filename)
            else:
                strm_Dict[name]['goes16_files'] = [strm_Dict[name]['goes16_files'],current_filename]   # places current file name in dictionary
            
            
            dir_path = '/data/jpk237/goes16-data'
            os.chdir(dir_path)
            if False == path.exists(current_filename):  # checks to see if not downloaded yet
                #print(dcount)
                dcount = dcount+1
                s3.Bucket(nc_file.bucket_name).download_file(nc_file.key,current_filename) # downloads current file
            #else:
                #print('already downloaded')
#####################################################



dataFile = "/data/jpk237/storm-data/IBTrACS.since1980.v04r00.nc"
DS_storms = xr.open_dataset(dataFile)

s_num = DS_storms.storm  #len(storms) # number of storms recorded
basin = 'NA'
lst_8_yrs = np.datetime64('2012-01-01T00:00:00.000040448')   # datetime of start of data collection
strt_date = np.datetime64('2019-01-01T00:00:00.000040448')   # start looping through at given date (to help speed up download)
# for some reason when converting to int, having the ns precision makes a big difference
# ---> be careful about this... full magnitudes off
lst_8_yrs_epoch = lst_8_yrs.astype(int)
strt_date_epoch = strt_date.astype(int)

count = 0 # placeholder for array keeper storm indices
s_loc = np.zeros(1)     # s_loc contains the position values of each storm I want in DS_storms
for n in range(len(s_num)):   # loops through every storm in dataset s_num is total number of storms
    storm_basin = DS_storms.basin.values[n,:]   # basin of nth storm
    marker = str(storm_basin[0])    # converts to str
    time_int = int(DS_storms.time.values[n,0])  # converts starting time to epoch time
    if marker == "b'NA'" and time_int >= strt_date_epoch: #lst_8_yrs_epoch:   # checks basin and within past 8 years
        if count == 0:      
            s_loc[0] = n    # records storm number (index)
        else:
            s_loc = np.append(s_loc,n)      # s_loc is number of NA storms
        count = count + 1






strm_Dict = {}  # dictionary that holds all storm data/imagery
strm_names = 0  # counter
strm_Dict['names'] = ()
for n in range(len(s_loc)):
    ## Getting lat/lon points and s3 datafiles for storm 'n' with name "name"
    name = str(DS_storms.name.values[int(s_loc[n])])    # makes dict entry for each storm
    name = name[2:]; name = name[:-1]   # cuts out apostrophe characters
    strm_Dict[name] = {}
    strm_Dict[name]['goes16_files'] = {} # sets up place in directory for all goes16 file names to be stored
    
    # keeps track of names
    if type(strm_Dict['names']) == list:
        strm_Dict['names'].append(name)
    else:
        strm_Dict['names'] = [strm_Dict['names'],name]   # places current storm name in dictionary
            
    
    
    
    #if strm_names == 0:     # if/else statement sets up sub-dictionary that has every name str
    #    strm_Dict['names'] = name
    #    strm_names = 1
    #else: 
    #    strm_Dict['names'] = [strm_Dict['names'],name]
    
    times = DS_storms.time.values[int(s_loc[n])]      # all times for storm 'n'
    t_steps = []                        # Removes all 'NaT' from date_times
    for t in range(len(times)):
        if np.isnat(times[t]) == False:
            t_steps.append(times[t])    # t_steps is all measured times for storm 'n'
    
    
    for t in range(len(t_steps)):
        
        marker = 't_' + str(t)
        strm_Dict[name][marker] = {}
        # Assigns lat, lon, time value into dictionary for storm 'n' 
        strm_Dict[name][marker]['d_time'] = datetime.utcfromtimestamp(t_steps[t].astype(int)*1e-9)
        strm_Dict[name][marker]['lat'] = DS_storms.lat.values[int(s_loc[n]),int(t)] # lat value of storm 'n' for dt 't'
        strm_Dict[name][marker]['lon'] = DS_storms.lon.values[int(s_loc[n]),int(t)] # lon value of storm 'n' for dt 't'
        # Now have dictionary to grab s3 files for storm 'n' at time 't' (t_'t')
        
        fil_nam = strm_Dict[name][marker]['d_time'].strftime('ABI-L1b-RadF/%Y/%j/%H/')
        
        # Grab all goes16 files for measured hour time
        get_goesFile(fil_nam)
        
        # s3_file becomes files for corresponding time 't' (will just be the first one grabbed by get_goes?)
        #strm_Dict[name][marker]['s3_files'] = fil_nam   
    
    
    #t_steps = t_steps.notnull()



dir_path = '/data/jpk237/summer-2020'
os.chdir(dir_path)

#dict = strm_Dict
#json = json.dumps(dict)
#f = open("dict.json","w")
#f.write(json)
#f.close()

f = open("strm_Dict.pkl","wb")
pickle.dump(strm_Dict,f)
f.close()

#with open('/data/jpk237/summer-2020/strm_Dict','wt') as my_file:
#    my_file.write(json.dumps(strm_Dict,indent=2))



lat_strt=np.zeros(len(s_loc)); lon_strt=np.zeros(len(s_loc));   # initial arrays for
time_strt=["" for x in range(len(s_loc))]                       # lat, lon, time, name, category
#category = np.zeros(len(s_loc));    # var name = cma_cat
strm_name=["" for x in range(len(s_loc))]

for j in range(0,len(s_loc)):
    lat_strt[j] = DS_storms.lat.values[int(s_loc[j]),0] # gets starting lat, lon
    lon_strt[j] = DS_storms.lon.values[int(s_loc[j]),0]
    strm_name[j] = str(DS_storms.name.values[int(s_loc[j])])
    tim = DS_storms.time.values[int(s_loc[j]),0]    # starting time in datetime ns
    #dt = datetime.fromtimestamp(tim // 1000000000)
    #s = dt.strftime('%Y-%m-%d %H:%M:%S')
    time_strt[j] = tim


#storm_marker = np.array(lat_strt,lon_strt,s_loc) # beginning lat/lon and position in DS

#central_lat = 37.5; central_lon = -96
#extent = [-120,-70,24,50.5]
#central_lon = np.mean(extent[:2]); central_lon = np.mean(extent[2:])
#plt.figure(figsize=(12,6))
#ax = plt.axes(projection=ccrs.AlbersEqualArea(central_lon,central_lat))
#ax.set_extent(extent)
#ax.add_feature(ccrs.feature.OCEAN); ax.add_feature(ccrs.feature.LAND,edgecolo='black')
#ax.add_feature(ccrs.feature.LAKES,edgecolor='black')
#ax.gridlines()

ax = plt.axes(projection=ccrs.PlateCarree())
ax.stock_img()

plt.plot([lon_strt],[lat_strt],         # plots starting point of each storm
            color='blue',marker='o',
            markersize=1.5,
            transform=ccrs.Geodetic(),
            )
            
#plt.show()