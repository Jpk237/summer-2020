### Make data image 2 
from datetime import datetime, timezone
import cartopy.feature as cfeature
import cartopy.crs as ccrs
import matplotlib.pyplot as plt
from metpy.plots import ctables
from metpy.units import units
import metpy.calc as mpcalc
from netCDF4 import num2date
import numpy as np
from scipy.ndimage import gaussian_filter
#from siphon.catalog import TDSCatalog
import xarray as xr
from cartopy import crs
from wrf import latlon_coords, ll_to_xy, xy_to_ll
#from pyproj import Proj
import pyproj as proj
from netCDF4 import Dataset
import pickle
from matplotlib.patches import Circle
import os.path
from os import path


def brightness_temp(ds):
    # Gets infrared values
    ir_rad = ds.Rad
    # save needed values
    fk1 = ds.planck_fk1; fk2 = ds.planck_fk2
    bc1 = ds.planck_bc1; bc2 = ds.planck_bc2
    # calculates brightness temp
    T = fk2 / (np.log((fk1/ir_rad) + 1))
    # add scale and offset corretion
    ir_BT = bc2*T + bc1
    return(ir_BT)



pkl_file = open('/data/jpk237/summer-2020/strm_Dict.pkl','rb')
strm_Dict = pickle.load(pkl_file)
pkl_file.close()

strm_names = strm_Dict['names']

for n in range(len(strm_names)):    # for loop to make images for each storm
    
    time_bool = 0   # ends while loop once all time steps have been plotted for current strm name
    t = 0
    while time_bool == 0:
        
        t_str = 't_' + str(t)    # timestamp to open, changes at the end of each loop
        try:        # checks to see if current timestamp t_str exists
            strm_Dict[strm_names[n]][t_str]
        except KeyError:
            present = False
        else:
            present = True
        
        if present:
            dataFile = '/data/jpk237/goes16-data/' + strm_Dict[strm_names[n]][t_str]['s3_file']
        
            # sets up projections and data series
            ds = xr.open_dataset(dataFile)
            rad = ds.metpy.parse_cf('Rad')
            merc = crs.Mercator.GOOGLE
            dataproj = rad.metpy.cartopy_crs
            
            
            # Gets lat, lon, radius for storm 'n' at time 't_str'
            strm_lat = strm_Dict[strm_names[n]][t_str]['lat']
            strm_lon = strm_Dict[strm_names[n]][t_str]['lon']
            #strm_roci = strm_Dict[strm_names[n]][t_str]['radius']

            #radius_m = strm_roci*1852   # converts nautical miles to meters
            #scaled_radius = radius_m*2.5    # scales to make sure whole storm is enclosed

            
            # Grab coordinate data
            x = rad.x
            y = rad.y

            lat1 = 50; lon1 = -100;
            lat2 = -5; lon2 = -15;
            coord_1 = [lat1, lon1]
            coord_2 = [lat2, lon2]

            # Sets up bounding box
            p = proj.Proj(dataproj.proj4_params)
            x_c1, y_c1 = p(coord_1[1], coord_1[0])
            x_c2, y_c2 = p(coord_2[1], coord_2[0])
            bbox = [x_c1,y_c1,x_c2,y_c2]


            ir_BT = brightness_temp(ds)

            # start figure and set up axis
            fig = plt.figure(1, figsize=(20,10))
            ax = plt.subplot(111, projection=merc)
            ax.set_extent([lon1,lon2,lat1,lat2], crs=ccrs.PlateCarree())

            # use cartopy feature module to add coastlines
            ax.add_feature(cfeature.COASTLINE.with_scale('50m'), edgecolor='lightgrey',linewidths=.75)
            ax.add_feature(cfeature.BORDERS.with_scale('50m'), edgecolor='lightgrey',linewidths=.75)
            ax.add_feature(cfeature.STATES.with_scale('50m'),edgecolor='lightgrey',linewidths=.75)

            # get colormap from metpy with a range of brightness values
            ir_norm, ir_cmap = ctables.registry.get_with_range('ir_drgb_r',190,350)

            # Plot the BT data with a colorbar
            img = ax.imshow(ir_BT,extent=(x.min(),x.max(),y.min(),y.max()),transform=dataproj,origin='upper',cmap=plt.cm.gray_r)
            
            
            # uncomment lines below to add circle around storm 
            #circ = Circle((xstrm,ystrm),scaled_radius,transform=dataproj,edgecolor='red',facecolor="None")
            #ax.add_patch(circ)

            
            
            #plt.show()
            plt.gca().set_axis_off()
            plt.subplots_adjust(top=1,bottom=0,right=1,left=0,hspace=0,wspace=0)
            plt.margins(0,0)
            plt.gca().xaxis.set_major_locator(plt.NullLocator())
            plt.gca().yaxis.set_major_locator(plt.NullLocator())
            
            dir_path = '/data/jpk237/training-images'
            os.chdir(dir_path)
            if False == path.exists('/data/jpk237/training-images/'+strm_Dict[strm_names[n]][t_str]['s3_file']+'.png'):  # checks to see if not downloaded yet
                plt.savefig(strm_Dict[strm_names[n]][t_str]['s3_file']+'.png',bbox_inches='tight',pad_inches=0)
        
            plt.clf()
            t = t+1 # moves on to next timestep
        else:   # no data for current timestep 't_str' so ends while loop
            time_bool = 1
            





