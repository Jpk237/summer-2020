#### Creates image from satellite data
from datetime import datetime, timezone
import cartopy.feature as cfeature
import cartopy.crs as ccrs
import matplotlib.pyplot as plt
from metpy.plots import ctables
from metpy.units import units
import metpy.calc as mpcalc
from netCDF4 import num2date
import numpy as np
from scipy.ndimage import gaussian_filter
#from siphon.catalog import TDSCatalog
import xarray as xr
from cartopy import crs
from pyproj import Proj


#dataFile = '/data/jpk237/goes16-data/OR_ABI-L1b-RadF-M6C16_G16_s20201491250164_e20201491259484_c20201491259554.nc'
dataFile = '/data/jpk237/goes16-data/OR_ABI-L1b-RadF-M3C16_G16_s20171060608082_e20171060618460_c20171060618509.nc'
#dataFile = '/data/jpk237/goes16-data/OR_ABI-L1b-RadF-M6C16_G16_s20192371240216_e20192371249535_c20192371249592.nc'
ds = xr.open_dataset(dataFile)

# get projection info
rad = ds.metpy.parse_cf('Rad')
dataproj = rad.metpy.cartopy_crs
merc = crs.Mercator.GOOGLE
#dataproj = rad.metpy.cartopy_merc

# Grab coordinate data
x = rad.x
y = rad.y

map_lat1 = 42
map_lon1 = -76
p = Proj('epsg:3857')
map_x, map_y = p(map_lon1,map_lat1)

# Grab time
vtime = datetime.strptime(ds.time_coverage_end,'%Y-%m-%dT%H:%M:%S.%fZ')

ir_rad = ds.Rad
# save needed values
fk1 = ds.planck_fk1; fk2 = ds.planck_fk2
bc1 = ds.planck_bc1; bc2 = ds.planck_bc2
# calculates brightness temp
T = fk2 / (np.log((fk1/ir_rad) + 1))
# add scale and offset corretion
ir_BT = bc2*T + bc1


# start figure and set up axis
fig = plt.figure(1, figsize=(20,10))
ax = plt.subplot(111, projection=merc)
ax.set_extent([-135,-60,10,65], crs=ccrs.PlateCarree())

# use cartopy feature module to add coastlines
ax.add_feature(cfeature.COASTLINE.with_scale('50m'), edgecolor='lightgrey',linewidths=.75)
ax.add_feature(cfeature.BORDERS.with_scale('50m'), edgecolor='lightgrey',linewidths=.75)
ax.add_feature(cfeature.STATES.with_scale('50m'),edgecolor='lightgrey',linewidths=.75)

# get colormap from metpy with a range of brightness values
ir_norm, ir_cmap = ctables.registry.get_with_range('ir_drgb_r',190,350)

# Plot the BT data with a colorbar
#img = ax.imshow(ir_BT,extent=(x.min(),x.max(),y.min(),y.max()),origin='upper',cmap=ir_cmap, norm=ir_norm)
img = ax.imshow(ir_BT,extent=(x.min(),x.max(),y.min(),y.max()),transform=dataproj,origin='upper',cmap=plt.cm.gray_r)

plt.colorbar(img, orientation='vertical',pad=0,aspect=50,ticks=range(190,351,10))


# Add some titles
plt.title('GOES-16 Infrared Imagery ({:0.1f} um)'.format(ds.band_wavelength.data[0]),loc='left')
plt.title('{0:%Y-%m-%d %H:%M:%S}'.format(vtime),loc='right')

#show plot
#plt.tight_layout()
plt.show()

fig = plt.figure(1, figsize=(20,10))
ax = plt.subplot(111, projection=dataproj)
img = ax.imshow(ir_BT,extent=(x.min(),x.max(),y.min(),y.max()),origin='upper',cmap=ir_cmap,norm=ir_norm)
plt.show()


# Stefan-Boltzman
sb_constant = 5.67e-8
# Calc emitted energy
wv_rad = ds.Rad
wv_BT = (fk2 / (np.log((fk1/wv_rad)+1)) - bc1)/bc2
E = sb_constant*wv_BT**4

# Start figure and set up axis
fig = plt.figure(1, figsize=(20,10))
ax = plt.subplot(111,projection=dataproj)

# Add geopolitical lines
ax.add_feature(cfeature.COASTLINE.with_scale('50m'), edgecolor='lightgrey',linewidths=.75)
ax.add_feature(cfeature.BORDERS.with_scale('50m'), edgecolor='lightgrey',linewidths=.75)
ax.add_feature(cfeature.STATES.with_scale('50m'),edgecolor='lightgrey',linewidths=.75)

# Plot emitted energy based on SB
#im = ax.imshow(E, extent=(x.min(),x.max(),y.min(),y.max()),origin='upper',cmap=plt.cm.gray)

plt.colorbar(img, orientation='vertical',pad=0,aspect=50)

# Plot titles
plt.title('Goes-16 Infrared Imagery ({:0.1f} um) - Emitted Energy ($W/m^2$)'.format(ds.band_wavelength.data[0]),
            loc='left')
#plt.title('{0:%Y-%m-%d %H:%M:%S}'.format(vtime), loc='right')

plt.tight_layout()
plt.show()
