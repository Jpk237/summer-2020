#### Python code for wrf Post Processing
from datetime import datetime
import cartopy.crs as ccrs
import matplotlib.pyplot as plt
import numpy as np
import xarray as xr
import cartopy.feature as cfeature
from cartopy.feature import NaturalEarthFeature
import matplotlib.gridspec as gridspec
from netCDF4 import Dataset
from matplotlib.cm import get_cmap
from wrf import (getvar,interplevel,to_np,latlon_coords,get_cartopy,
                cartopy_xlim,cartopy_ylim)

# Ithaca lat/lon 42.4440° N, 76.5019° W

dataFile = "/data/wrfout/wrfout_d01_2020-06-22_00:00:00"
DS_wrf = xr.open_dataset(dataFile)
t1 = 10; t2 = 24  # t1 --> 8am day one, t2 --> 8am day two
ilat = 42.4440; ilon = -76.5019  # lat lon coords of ithaca

def geo_idx(dd, dd_array):
    # Search for nearest decimal degree in an array
    geo_idx = (np.abs(dd_array - dd)).argmin()
    return geo_idx

lats = DS_wrf.variables['XLAT'][:]
lons = DS_wrf.variables['XLONG'][:]

lat_diff = lats.values[0,:,0] - ilat;
lon_diff = lons.values[0,0,:] - ilon;
lat_idx = (np.absolute(lat_diff)).argmin() # finds lat/lon value closest to ithaca
lon_idx = (np.absolute(lon_diff)).argmin()

#lat_idx = geo_idx(ilat,lats)
#lon_idx = geo_idx(ilon,lons)

#wrf_T2 = DS_wrf.T2.values[t1:t2,:,:] # two meter temp
max_T2 = DS_wrf.T2.values[t1:t2,lat_idx,lon_idx].max()
print('The max temp is ' + str(max_T2) + ' K')

wrf_pres = DS_wrf.PSFC.values[t1:t2,(lat_idx-4):(lat_idx+4),(lon_idx-4):(lon_idx+4)] # surface pressure
max_pres = wrf_pres.max()
print('The max pressure is ' + str(max_pres) + ' Pa')

wrf_hum = DS_wrf.Q2.values[t1:t2,:,:] # two meter specific humididty
max_hum = wrf_hum.max()
print('The max humidity is ' + str(max_hum))


temp2 = DS_wrf.variables['T2'][t1,:,:]

fig = plt.figure(figsize=(12,6))
ax = plt.axes(projection=ccrs.PlateCarree())
states = NaturalEarthFeature(category="cultural",scale='50m',
                            facecolor="none",
                            name="admin_1_states_provinces_shp")
ax.add_feature(states, linewidth=.5,edgecolor="black")
ax.coastlines('50m',linewidth=0.8)

plt.contourf(DS_wrf.XLONG.values[t1,:,:],DS_wrf.XLAT.values[t1,:,:],DS_wrf.T2.values[t1,:,:],10,
            cmap='bwr',
            transform=ccrs.PlateCarree())
            
plt.colorbar(ax=ax,shrink=.98)
plt.title("Two-Meter Temp")
plt.show()


#p = DS.
#ht_500 = interplevel(z,p,500)

