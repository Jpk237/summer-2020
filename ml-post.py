import boto3
from boto3.session import Session
import io
from PIL import Image, ImageDraw, ExifTags, ImageColor
import os.path
from os import path
import numpy as np

aws = Session()
s3 = aws.resource('s3')
jpk237_satellite_images = s3.Bucket('jpk237-satellite-images')

def detect_labels_local_file(photo,arn,bucket):

    client=boto3.client('rekognition',region_name='us-east-1')
    #Load image from S3 bucket
    s3_connection = boto3.resource('s3')
    s3_object = s3_connection.Object(bucket,photo)
    s3_response = s3_object.get()
    stream = io.BytesIO(s3_response['Body'].read())
    image=Image.open(stream)   
    
    
    # call ML algorithm
    #with open(photo, 'rb') as image:
    #    response = client.detect_custom_labels(ProjectVersionArn=arn,Image={'Bytes': image.read(),
    #                                        'S3Object':{'Bucket': 'jpk237-satellite-images','Name':'alt-training/OR_ABI-L1b-RadF-M6C16_G17_s20191770600340_e20191770609418_c20191770609457.nc.png'}})
    
    response = client.detect_custom_labels(ProjectVersionArn=arn,Image={'S3Object': {'Bucket': bucket, 'Name': photo}})
    #print(response)
    dict = response
    
    imgWidth, imgHeight = image.size  
    draw = ImageDraw.Draw(image)
    
    print('Detected labels in ' + photo)    
    #for label in response['TropicalStorm']:
    #    print (label['Name'] + ' : ' + str(label['Confidence']))
        
    #for TropicalStorm in response['CustomLabels']:
        #print (label['Name'] + ' : ' + str(label['Confidence']))
        #box = TropicalStorm['Name']['BoundingBox']
    box = response['CustomLabels'][0]['Geometry']['BoundingBox']
    #print(box)
    left = imgWidth * box['Left']
    top = imgHeight * box['Top']
    width = imgWidth * box['Width']
    height = imgHeight * box['Height']
    
    points = (
        (left,top),
        (left + width, top),
        (left + width, top + height),
        (left , top + height),
        (left, top)

    )
    draw.line(points, fill='#00d400', width=2)
    
    #fontsize = 20
    #font = ImageFont.truetype("arial.ttf", fontsize)
    draw.text((0, 0),"Confidence: "+str(response['CustomLabels'][0]['Confidence']),(255,255,255))#,font=font)
    
    dir_path = '/data/jpk237/tested-images'
    os.chdir(dir_path)
    photoname = 'tested-'+photo[13:]
    if False == path.exists('/data/jpk237/tested-images/'+photoname): #checks to see if not downloaded yet
                
                image.save(photoname)

    confidence = response['CustomLabels'][0]['Confidence']
    #print((confidence))
    return confidence
    
    


#photo= "alt-training/OR_ABI-L1b-RadF-M6C16_G17_s20191770600340_e20191770609418_c20191770609457.nc.png"
arn = 'arn:aws:rekognition:us-east-1:043149540045:project/sat-images/version/sat-images.2020-07-22T15.38.58/1595446740019'
bucket = "jpk237-satellite-images"





pref='alt-training'
count = 0; num = 0
for im_number in jpk237_satellite_images.objects.filter(Prefix=pref):
    photo = im_number.key
    
    if num == 0:    # first im_num is just 'alt-trianing/' so this skips it
        num = num +1
    else:
        label_confidence = detect_labels_local_file(photo,arn,bucket)
    
        if count == 0:
            confidence_list = np.array(label_confidence)
            count = count + 1
        else:
            confidence_list = np.append(confidence_list,label_confidence)

