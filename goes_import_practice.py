from datetime import datetime, timezone
from os import path
import re
from boto3.session import Session
import numpy as np
import xarray as xr
from datetime import datetime
import boto3
import os.path
from os import path


aws = Session()
s3 = aws.resource('s3')
S3 = boto3.client('s3')
noaa_goes16 = s3.Bucket('noaa-goes16')
noaa_goes16.creation_date

#goes_dwnld = 'dwnld file'
#goes_file_path = '/data/jpk237/goes_file.nc'

#if not path.exists(goes_file_path)
#    goes_dwnld.download_file(Filename=goes_file_path)



#goes16_datetime = re.compile(r'OR_ABI-L1b-RadF-M\dC14_G16_s(\d{14})_e\d{14}_c\d{14}\.nc')

#random_goes16 = 'OR_ABI-L1b-RadF-M3C01_G16_s20190010000364_e20190010011131_c20190010011177.nc'


#my_filename = path.basename(random_goes16)  #reduces directory name?
#goes_channel = afsddasf

#if goes_channel not in ['14,'15']:

#goes_groups = goes16_datetime.search(random_goes16).groups()

#goes_scan_start = goes_groups[0][:-3]
#goes_scan_start = datetime.strptime(goes_scan_start, '%Y%j%H%M').replace(tzinfo=timezone.utc)



#print(goes_scan_start.isoformat())

## look at channel 14 (clean infrared band) M6C14 (at to reg expression)
# OR_ABI-L1b-RadF-M\dC16_G16_s(\d{14})_e\d{14}_c\d{14}\.nc  prefix = 'ABI-L1b-RadF/2020/032/18/'


def get_goesFile(pref):
    ### Grabs all goes16 files under time 'pref'---> fro IBtRACS hour datapoint, now have 10/15 minute imagery
    for nc_file in noaa_goes16.objects.filter(Prefix=pref):
        goes16_datetime = re.compile(r'OR_ABI-L1b-RadF-M\dC16_G16_s(\d{14})_e\d{14}_c\d{14}\.nc')
        random_goes16 = nc_file.key
        current_filename = path.basename(random_goes16)
        if None != goes16_datetime.search(current_filename):
            goes_groups = goes16_datetime.search(current_filename).groups()
            goes_scan_start = goes_groups[0][:-3]
            goes_scan_start = datetime.strptime(goes_scan_start, '%Y%j%H%M').replace(tzinfo=timezone.utc)
            ## change directory path to data location
            ## if statement: check if downloaded
            print('Downloaded if not already')
            print('Download path for {0}'.format(nc_file.key))


dataFile = "/data/jpk237/storm-data/IBTrACS.since1980.v04r00.nc"
DS_storms = xr.open_dataset(dataFile)
s_num = DS_storms.storm
time = DS_storms.time.values[4359]

for t in range(0,3):
    t_0 = time[t]
    dt = datetime.utcfromtimestamp(t_0.astype(int)*1e-9)
    #tt = dt.timetuple()
    pref = 'ABI-L1b-RadF/' + dt.strftime("%Y") + '/' + dt.strftime("%j") + '/' + dt.strftime("%H") + '/'

    #get_goesFile(pref)
    #print('TIME DONE')

for nc_file in noaa_goes16.objects.filter(Prefix=pref):
        goes16_datetime = re.compile(r'OR_ABI-L1b-RadF-M\dC16_G16_s(\d{14})_e\d{14}_c\d{14}\.nc')
        random_goes16 = nc_file.key
        current_filename = path.basename(random_goes16)
        if None != goes16_datetime.search(current_filename):
            goes_groups = goes16_datetime.search(current_filename).groups()
            goes_scan_start = goes_groups[0][:-3]
            goes_scan_start = datetime.strptime(goes_scan_start, '%Y%j%H%M').replace(tzinfo=timezone.utc)
            ## change directory path to data location
            ## if statement: check if downloaded
            print('Downloaded if not already')
            print('Download path for {0}'.format(nc_file.key))

dir_path = '/data/jpk237/goes16-data'
os.chdir(dir_path)
fil_nam = str(nc_file.key)
if True != path.exists(current_filename):
    s3.Bucket(nc_file.bucket_name).download_file(nc_file.key, current_filename)
else:
    print('afsd')

#S3.download_file(Bucket=nc_file.bucket_name,Key=nc_file.key)#,Filename=fil_nam)
